from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from ftplib import FTP
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse,HttpResponseRedirect
import subprocess
from django.conf import settings
from django.core.files.base import ContentFile
from django.utils.encoding import smart_str
from django.core.servers.basehttp import FileWrapper
import os
import getpass
# Comentario
rutaMedia = "/home/erik/servidorftp/Prueba/media/"
servidor = ""
ruta = ""
temp = ""
user = ""
password = ""
desktop = ""
conexion = None
# Create your views here.
servidor = os.popen("hostname -I").readline()
servidor = servidor.split("\n")[0]

# Metodo para separar la ruta
def VSplit(param):
    temp = param
    t = temp.rsplit("/",1)
    t[0] = t[0] + "/"
    return t

def grabFile(rutas,s):
    global rutaMedia
    conexion = s
    path = rutas[0]
    filename = rutas[1]
    # filename
    localfile = open(rutaMedia+ 'media/' + filename,'wb')
    conexion.retrbinary('RETR ' + filename, localfile.write)
    localfile.close()

def placeFile(rutas,s):
    conexion = s
    path = rutas[0]
    filename = rutas[1]
    conexion.storbinary('STOR '+filename, open(path+filename,'rb'))

def SubirFile(conexion,f):
    ruta = str(f)
    arreglo = VSplit(ruta)
    placeFile(arreglo,conexion)

def BajarFile(conexion,f):
    ruta = f
    arreglo = VSplit(ruta)
    grabFile(arreglo,conexion)

# Vista Inicial del login
def Index(request):
    global desktop,conexion
    datos = []
    #conexion.dir(datos.append)
    return render(request,"Vista.html",{'ruta':datos})

def Datos(u,p,s):
    global user,password,server
    user = u
    password = p
    server = s

def Conexion_Ftp():
    global user,password,server
    conexion = None
    desktop = ""
    arreglo = []
    try:
        conexion = FTP(server)
        conexion.login(user,password)
        print("[+] Conexion establecida correctamente")
        desktop = conexion.pwd()
        # print(desktop)
        conexion.cwd(desktop)
        # t = conexion.nlst()
        # print(t)
    except Exception as e:
        print("[-] No se pudo establecer la conexion al servidor" + str(e))
    arreglo = [desktop,conexion]
    return arreglo

def login_view(request):
    # Credenciales
    global servidor
    u = request.POST.get('username')
    p = request.POST.get('password')
    #s = request.POST.get('server')
    s = servidor
    # Arreglo para obtener la conexion y la ruta de la carpeta del ftp
    t = []
    # Metodo para llenar las variables globales con los valroes obtenidos de las Credenciales
    Datos(u,p,s)
    # Variables Globales
    global conexion,desktop
    t = Conexion_Ftp()

    # Conexion FTP
    desktop = t[0]
    conexion = t[1]

    # Autenticacion del usuario
    try:
        user = authenticate(username=u, password=p)
    except:
        print "*Salio Error*"
    else:
        print "*Todo bien*"
    finally:
        print user

    # Loguear
    if user is not None:
        login(request, user)
        return render(request,"Vista.html",{'ruta':desktop})
    else:
        return render(request,"Vista.html")

def logout_view(request):
    logout(request)
    return render(request,"Vista.html")

def Ver_View(request):
    global conexion,desktop
    conexion.cwd('/home/erik/Escritorio')
    files = []
    files = conexion.nlst()

    ctx = {
    "items":files,
    "ruta":desktop
    }
    return render(request,"Ver.html",ctx)

def Descargar(request,string):
    global conexion,desktop, rutaMedia
    filepath = ""
    filepath = desktop + "/" + string
    BajarFile(conexion,filepath)
    filepath = rutaMedia+ 'media/' + string
    wrapper = FileWrapper(open(filepath, "r" ))
    response = HttpResponse(wrapper,content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(string)
    response['X-Sendfile'] = smart_str(filepath)
    return response

def file_View(request):
    if request.method=='POST':
        global conexion,desktop,rutaMedia
        folder = request.path.replace("/", "_")
        uploaded_filename = request.FILES['file'].name
        # create the folder if it doesn't exist.
        try:
            os.mkdir(os.path.join(settings.MEDIA_ROOT, folder))
        except:
            pass
        # save the uploaded file inside that folder.
        full_filename = os.path.join(settings.MEDIA_ROOT, folder, uploaded_filename)
        fout = open(full_filename, 'wb+')
        file_content = ContentFile(request.FILES['file'].read())
        # Iterate through the chunks.
        for chunk in file_content.chunks():
            fout.write(chunk)
        fout.close()
        f = rutaMedia +"_file_/" + uploaded_filename
        SubirFile(conexion,f)
        return render(request,"Vista.html")
    return render(request,"Files.html")
