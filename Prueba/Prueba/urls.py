from django.conf.urls import include, url
from django.contrib import admin
from app import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'Prueba.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.Index, name='index_View'),
    # url(r'^TestPrueba/$', views.Example, name='test_view'),
    url(r'^login/$', views.login_view, name='log_View'),
    url(r'^logout/$', views.logout_view, name='logout_View'),
    url(r'^file/$', views.file_View, name='file_View'),
    url(r'^Ver/$', views.Ver_View, name='ver_View'),
    url(r'^Descargar/(?P<string>.+)/$', views.Descargar, name='descargar_view'),
]
